#!/usr/bin/env python3

import pandas as pd
import argparse
import json


def merge_Final_results(resultFiles, algFileName):
	output_list = []

	for i, result_file in enumerate(resultFiles):
		with open(result_file, "rb") as infile:
			output_list.append(json.load(infile))
	
	with open(algFileName, "w") as outfile:
		json.dump(output_list, outfile,indent=4)



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Aggregate Results of all the algorithms in One File')
	parser.add_argument("--f", type=str, help="path of the final result file")
	parser.add_argument("--r", action='store', dest='listparamsindicators',type=str, nargs='*', default=[],help="List of file indicators")
	args = parser.parse_args()
	merge_Final_results(resultFiles=args.listparamsindicators,
		algFileName=args.f
		)
