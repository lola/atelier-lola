#!/usr/bin/env python3

from surprise import accuracy
import argparse
import sys
import pandas as pd
import re
import json

def calculate_indicators(true_labels, predicted_labels,filenameResults,listIndicators):
	counter=re.search(r"(\w*)_(\w*)", predicted_labels)
	counter_str=counter.group(1)
	df_true_label=pd.read_csv(true_labels, delimiter=';')
	df_predicted=pd.read_csv(predicted_labels)
	df_predicted['rui']	= df_true_label['rui']

	dataJson={}
	dataJson[counter_str]=[]
	for indicator in listIndicators:
		if(indicator=="rmse"):
			calculate_indicator(df_predicted,'RMSE',accuracy.rmse,dataJson[counter_str])
		elif(indicator=="fcp"):
			calculate_indicator(df_predicted,'FCP',accuracy.fcp,dataJson[counter_str])
		elif(indicator=="mae"):
			calculate_indicator(df_predicted,'MAE',accuracy.mae,dataJson[counter_str])
		elif(indicator=="mse"):
			calculate_indicator(df_predicted,'MSE',accuracy.mse,dataJson[counter_str])
			
	filename=filenameResults+counter_str+".json"
	with open(filename, 'w') as outfile:
		json.dump(dataJson,outfile,indent=4)		

def calculate_indicator(predict_df, indicator, func,datadf):
	data=[]	
	predict_list = predict_df.values.tolist()
	computedindicator=func(predict_list)
	data.append([1,computedindicator])
	df_path = pd.DataFrame(data, columns = ['predicted_labels', indicator])
	datadf.append({indicator:df_path[indicator].tolist()})


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Calculate  indicators')
	parser.add_argument('--r', '--reallabelFile',required=True, type=str,help="File of real labels")
	parser.add_argument('--p', '--predictFile',required=True, type=str,help="File of predictions")
	parser.add_argument('--f', required=True, type=str, help="The name of the result file")
	parser.add_argument('--i', action='store', dest='listparamsIndicators',type=str, nargs='*', default=[],help="List of indicators to calculate")
	if len(sys.argv) == 1:
		parser.print_help()
		sys.exit()
	args = parser.parse_args()
	calculate_indicators(
		true_labels=args.r,
		predicted_labels=args.p,
		filenameResults=args.f,
		listIndicators=args.listparamsIndicators
		)


