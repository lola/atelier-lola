#!/usr/bin/env python3

from surprise import NormalPredictor
import pickle
import argparse
import pandas as pd 
import sys
from surprise import Reader
from surprise import Dataset

def train_NormalPredictor(train_data,trained_model_name):
	"""
	Train the normal Predictor Model on the data collected from the LRS
	"""

	model_normalPred = NormalPredictor()
	df_train=pd.read_csv(train_data)
	# As we're loading a custom dataset, we need to define a reader.
	reader = Reader(line_format='user item rating', sep=',',rating_scale=(0, 1))
	data = Dataset.load_from_df(df_train, reader=reader)
	trainset=data.build_full_trainset()
	model_normalPred.fit(trainset)
	#save the trained model
	filename = trained_model_name+".sav"
	pickle.dump(model_normalPred, open(filename, 'wb'))
		

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Train the NormalPredictor model')
	parser.add_argument("--a",type=str,help=" The Path to trainset file")
	parser.add_argument("--m", type=str, help="The filename to save the trained model")
	if len(sys.argv)==1:
		parser.print_help()
		sys.exit()
	
	args = parser.parse_args()
	train_NormalPredictor(
		train_data=args.a,
		trained_model_name=args.m
		)
