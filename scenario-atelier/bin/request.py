#!/usr/bin/env python3

import argparse
import requests
import json
import csv
import pandas as pd
from sklearn.preprocessing import MinMaxScaler



def parse_var(s):
    items = s.split('=')
    key = items[0].strip() #remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)

def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d


def build_user_request (headers, params, auth, url,port, csvFile):
    """
    Connect to the LRS and get the xAPI in a Json Format
    """
    more=True
    if not url.startswith("http://"):
        url = f"http://{url}"
    print(url)
    response = requests.get(url=url+":"+port+"/trax/ws/xapi/statements", auth=auth, headers=headers, params=params)
    data = response.json()
    fdata=data['statements']
    while (data.get('more')):
        response = requests.get(url=data['more'], auth=auth, headers=headers, params=params)
        data = response.json()
        fdata.extend(data['statements'])
    
    prepare_data(csvFile,fdata)


#create the Utility matrix: User, Item, Score, Timestamp (if exist)
def prepare_data (csvFile, listjson):
    #First parse all entries to get the complete fieldname list
    print(type(listjson))
    fieldnames = set()
    for entry in listjson:
        fieldnames.update(get_leaves(entry).keys())
    df = pd.DataFrame(columns=list(fieldnames), dtype=object)
    #parse each entry to get the required features rows
    df=pd.DataFrame(get_leaves(entry) for entry in listjson)
    reorder_columns=['actor', 'item', 'num_click']
    df=df.reindex(columns=reorder_columns)
    #normalize the "num_click" using MinMaxScaler
    df[['num_click']] = MinMaxScaler().fit_transform(df[['num_click']])
    #write rows to a csv file
    df.to_csv(csvFile, index=False)

#parse json file 
def get_leaves(item, key=None):

    if isinstance(item, dict):
        leaves = {}
        for i in item.keys():
            if(i!='stored' and i!='verb' and i!='authority' and i!='version' and i!='timestamp'):
                leaves.update(get_leaves(item[i], i))
        return leaves
    elif isinstance(item, list):
        leaves = {}
        for i in item:
            leaves.update(get_leaves(i, key))
        return leaves
    else:
        if(key=='mbox'):
            key='actor'
        elif(key=='id'):
            key='item'
        elif(key=='http://open.ac.uk/click'):
            key='num_click'
        return {key : item}

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Request GET to Trax LRS and extract required features from the database")
    parser.add_argument("--h","--headers",metavar="KEY=VALUE",nargs='+',help="headers of the GET request")
    parser.add_argument("--p","--parameters",metavar="KEY=VALUE",nargs='+',help="parameters of the requets")
    parser.add_argument("--a","--auth", action='store', dest='authparams',type=str, nargs='*', default=[],help="Basic Authentication parameters")
    parser.add_argument("--url", type=str, help="TRAX LRS url")
    parser.add_argument("--port", type=str, help="TRAX LRS port")
    parser.add_argument("--c","--csv", type=str, help="Path to the CSV File that contains the Traun data to be used in the scenario")
    args = parser.parse_args()
    build_user_request(
        headers=parse_vars(args.h), 
        params=parse_vars(args.p),
        auth=tuple(args.authparams),
        url=args.url,
        port=args.port,
        csvFile=args.c
        )
