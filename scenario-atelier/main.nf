/* 
 * enables modules 
 */
nextflow.enable.dsl = 2

// Include Process to retrieve Data
include { requestData } from "$projectDir/modules/retrieveData"

// Include SVD related Processes: Train, Test , Evaluate 
include { svd_train } from "$projectDir/modules/svdProcess"
include { model_test as svd_test } from "$projectDir/modules/testProcess"
include { evaluateP as svd_eval} from "$projectDir/modules/evaluateProcess"

// Include SVDpp related Processes: Train, Test , Evaluate 
include { svdpp_train } from "$projectDir/modules/svdppProcess"
include { model_test as svdpp_test } from "$projectDir/modules/testProcess"
include { evaluateP as svdpp_eval} from "$projectDir/modules/evaluateProcess"

// Include NormalPredictor related Processes: Train, Test , Evaluate 
include { normalP_train } from "$projectDir/modules/normalPredProcess"
include { model_test as normalP_test } from "$projectDir/modules/testProcess"
include { evaluateP as normalP_eval} from "$projectDir/modules/evaluateProcess"

// Include process to merge all results in one file
include { mergeResProcess } from "$projectDir/modules/algoAggResults"


workflow {
    /*
    *   Input Channels for the processes
    */
    dataChannel=Channel.from(params.csvFile)
    // Contains the name of the saved trained model
    svdTrainChannel=Channel.from(params.svd_trained_model)
    svdppTrainChannel=Channel.from(params.svdpp_trained_model)
    normalPredTrainChannel=Channel.from(params.normalP_trained_model)

    // Contains the name of the file to save SVD predictions 
    svdpredictionChannel=Channel.from(params.svd_predict_filename)
    svdpppredictionChannel=Channel.from(params.svdpp_predict_filename)
    normalPredpredictionChannel=Channel.from(params.normalP_predict_filename)

     // Contains the path of the test data 
    testdataChannel=Channel.from(params.test_data)
    // Contains the path of the true labels
    truelabelsChannel=Channel.from(params.true_labels)
    // Contains the name of the file to save indicators: used by all processes
    indicatorsFirstinputChannel= Channel.from(params.fileName)
    //Define empty channels to capture the results of evaluation of each algorithm
    svdresultChannel=Channel.empty()
    svdppresultChannel=Channel.empty()
    normalPredresultChannel=Channel.empty()

    //create channel that points out the name of the results filename
    fileResultChannel=Channel.from(params.fileResults)
   
    /*
    *   Retrieve DATA
    * 
    */
    requestData(dataChannel)
    
    
    /*
    *   Train, Test and Evalutae The SVD model
    * 
    */

    if(params.svdflag){
        svd_train(requestData.out.channel_data,svdTrainChannel)
        svd_test(svd_train.out.svdtrainedmodel,testdataChannel,svdpredictionChannel)
        svd_eval(truelabelsChannel,svd_test.out.modelpredictions,indicatorsFirstinputChannel) 
        svdresultChannel=svd_eval.out.indicatoroutputChannel
    }

    /*
    *   Train, Test and Evalutae The SVDpp model
    * 
    */
    if(params.svdppflag){
        svdpp_train(requestData.out.channel_data,svdppTrainChannel)
        svdpp_test(svdpp_train.out.svdpptrainedmodel,testdataChannel,svdpppredictionChannel)
        svdpp_eval(truelabelsChannel,svdpp_test.out.modelpredictions,indicatorsFirstinputChannel)
        svdppresultChannel=svdpp_eval.out.indicatoroutputChannel

    }

    /*
    *   Train, Test and Evalutae The Normal Predictor model
    * 
    */
    if(params.normalpredictorflag){
        normalP_train(requestData.out.channel_data,normalPredTrainChannel)
        normalP_test(normalP_train.out.normalPtrainedmodel, testdataChannel,normalPredpredictionChannel)
        normalP_eval(truelabelsChannel,normalP_test.out.modelpredictions,indicatorsFirstinputChannel)
        normalPredresultChannel=normalP_eval.out.indicatoroutputChannel
   }


    /*
    * Merge Results in one File
    *
    */
   resultChannel=normalPredresultChannel.mix( svdppresultChannel, svdresultChannel)
    
    mergeResProcess(resultChannel.toList(), fileResultChannel)
}
