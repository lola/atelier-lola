# Scenario-Atelier

Scenario pour l'atelier du 28-04-2022.

## requirements

- nextflow >= 21.04.3
- **Si utilisation des scripts dans Docker**
  - docker >= 20.04.14
- **Si utilisation des scripts directement via Python**
  - python >= 3.8
  - Dépendances Python (`pip install -r requirements.txt`)
  
## Build Docker Image

```BASH
$ docker build -t recommandation:latest .
```

## Utilisation du scénario avec Docker

```Bash
$ nextflow -c nextflow_docker.config run main.nf --
```

Possible options:

- `--lrsHost`
  URL of the LRS 
- `--lrsPort`
  Port of the LRS 
- `--datasetName`
  The name of the dataset to be used with this scenario
- `--rmse`
  The name of the indicator RMSE
- `--fcp`
  The name of the indicator FCP
- `--mae`
  The name of the indicator MAE
- `--mse`
  The name of the indicator MSE
- `--svdflag`
  conditional Variable to trigger the execution of SVD Algorithm
- `--svdppflag`
  conditional Variable to trigger the execution of SVDpp Algorithm
- `--baselineOnlyflag`
  conditional Variable to trigger the execution of Baseline-Only Algorithm
- `--normalpredictorflag`
  conditional Variable to trigger the execution of Normal Predictor Algorithm
