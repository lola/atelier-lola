#!/usr/bin/env nextflow
/*
 * Process to evaluate the model ability to predict on unkown data 
*/

process evaluateP{
    cpus 8
    memory '6G'
    container = "$params.main_container"
    input:
    path x
    path y
    val z
    output:
    path "$z*", emit:indicatoroutputChannel
    
    """
    evaluate.py  --r $x --p $y --f $z --i $params.rmse $params.fcp $params.mae $params.mse
    """


}
