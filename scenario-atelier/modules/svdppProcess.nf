#!/usr/bin/env nextflow
/*
 * Process to train SVDpp on data collected from the LRS
*/
process svdpp_train{
    cpus 8
    memory '6G'
    container = "$params.main_container"
    input:
    path x
    val z
    output:
    path "$z*" , emit: svdpptrainedmodel

    """
    svdpp_train.py --p $params.svdpp_n_factors $params.svdpp_n_epochs $params.svdpp_init_mean $params.svdpp_init_std_dev $params.svdpp_lr_all $params.svdpp_reg_all $params.svdpp_lr_bu $params.svdpp_lr_bi $params.svdpp_lr_pu $params.svdpp_lr_qi $params.svdpp_lr_yj $params.svdpp_reg_bu $params.svdpp_reg_bi $params.svdpp_reg_pu $params.svdpp_reg_qi $params.svdpp_reg_yj $params.svdpp_random_state  --a $x --m $z 
    """
}

