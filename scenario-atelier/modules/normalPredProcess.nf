#!/usr/bin/env nextflow
/*
 * Process to train Normal Predictor on data collected from the LRS
*/
process normalP_train{
    cpus 8
    memory '6G'
    container = "$params.main_container"

    input:
    path traindata
    val filename
    output:
    path "$filename*" , emit: normalPtrainedmodel

    """
    normalPredictor_train.py --a $traindata --m $filename
    """
}


