#!/usr/bin/env nextflow
/*
 * Process to aggregate results for one Algorithm
*/

process mergeResProcess{
    cpus 8
    memory '6G'
    container = "$params.main_container"
    input:
    path x 
    val y
    output:
    path "$y" , emit: alg_FinalResults
"""
mergeResults.py --r $x --f $y
"""
}
