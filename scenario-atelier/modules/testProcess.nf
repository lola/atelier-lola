#!/usr/bin/env nextflow
/*
 * Process to test models on datasets 
*/

process model_test{
    cpus 8
    memory '6G'
    container = "$params.main_container"
    input:
    path x
    path y
    val z
    output:
    path "$z*" , emit: modelpredictions

    """
    model_test.py --m $x --t $y --f $z
    """
}
