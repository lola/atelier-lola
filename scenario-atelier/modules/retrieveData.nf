#!/usr/bin/env nextflow
/*
 * Process to retrieve data from the LRS
*/

process requestData{
    cpus 8
    memory '6G'
    container = "$params.main_container"
    input:
    val x 
    output:
    path "$x" , emit: channel_data
"""
request.py --h $params.header_1=$params.valueheader_1 --p $params.param_1=$params.valueparam_1 $params.param_2=$params.valueparam_2 --a $params.authentificationUser $params.authentificationPassw --url $params.lrsHost --port $params.lrsPort  --c $x
"""
}
